#include<Stdio.h>
#include<stdlib.h>
#include <string.h>

/*Input a string from user on command line. String may have multiple commas e.g.
"Welcome,to,Sunbeam,CDAC,Diploma,Course". Print each word individually. Hint: use strtok()
function.
*/

int main() 
{ 
    char str[] = "Welcome,to,Sunbeam,CDAC,Diploma,Course"; 
  
    // Returns first token 
    char* token = strtok(str, ","); 
  
    // Keep printing tokens while one of the 
    // delimiters present in str[]. 
    while (token != NULL) { 
        printf("%s\n", token); 
        token = strtok(NULL, ","); 
    } 
  
    return 0; 
}