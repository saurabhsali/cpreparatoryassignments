#include <stdio.h>
#include <stdlib.h>
#define ARR_SIZE 10

int displayChoice();
void addElement(int[]);
void displayArray(int[]);
void deleteElement(int[]);
void maxElement(int[]);
void minElement(int[]);
void sumOfElements(int[]);

int main(void) {
	int arr[ARR_SIZE]={0,0,0,0,0,0,0,0,0,0};
	int choice;
	setvbuf(stdout,NULL,_IONBF,0);
	do{
		choice = displayChoice();
		switch(choice)
		{
			default:
				printf("\n invalid case ");
				break;
			case 1:
				addElement(arr);
				break;
			case 2:
				deleteElement(arr);
				break;
			case 3:
				maxElement(arr);
				break;
			case 4:
				minElement(arr);
				break;
			case 5:
				sumOfElements(arr);
				break;
			case 6:
				displayArray(arr);
				break;
			case 7:exit(0);
		}
	}while(choice!=7);
	return 0;
}

int displayChoice(){
	int choice;
	setvbuf(stdout,NULL,_IONBF,0);
	printf("\n\n1. add element to the array");
	printf("\n2. delete element from array");
	printf("\n3. find max. element");
	printf("\n4. find min. element");
	printf("\n5. sum of elements");
	printf("\n6. display elements");
	printf("\n7. Exit");
	printf("\n Enter your choice:");
	scanf("%d",&choice);
	return choice;
}

int traverseIndex(int arr[])
{
	int i;
	int isFull=1;
	printf("\n\nIndexes available are : ");
	for(i=0;i<ARR_SIZE;i++)
	{
		if(arr[i]==0)
		{
			printf("%d ",i);
			isFull=0;
		}
	}
	return isFull;
}

void addElement(int arr[])
{
	int isFull,index,number;
	isFull=traverseIndex( arr);
	if(isFull==1)
	{
		printf("\nArray is full");
		return;
	}
	setvbuf(stdout,NULL,_IONBF,0);
	printf("\nEnter the index :");
	scanf("%d",&index);
	printf("\nEnter the Element :");
	scanf("%d",&number);

	if(number<=0)
	{
		printf("\nInvalid number");
		return;
	}
	if(index>=0 && index<ARR_SIZE && arr[index]==0)
	{
		arr[index]=number;
		printf("\nElement is added");
	}
	else
	{
		printf("\nInvalid index");
	}
}

int traverseDelete(int arr[])
{
	int i;
	int isEmpty=1;

	for(i=0;i<ARR_SIZE;i++)
	{
		if(arr[i]!=0)
		{
			if(isEmpty==1)
			{
				printf("\n\nIndexes available for deletion are : ");
			}
			printf("\narr[i]=%d ",i);
			isEmpty=0;
		}
	}
	return isEmpty;
}

void deleteElement(int arr[])
{
	int isEmpty,index,number;
	isEmpty=traverseDelete( arr);
	if(isEmpty==1)
	{
		printf("\nArray is empty");
		return;
	}
	printf("\nEnter the index to be deleted : ");
	scanf("%d",&index);
	if(index>=0 && index<ARR_SIZE && arr[index]!=0)
	{
		number=arr[index];
		arr[index]=0;
		printf("\narr[%d]=%d is deleted",index,number);
	}
	else
	{
		printf("\nInvalid index");
	}
}

void maxElement(int arr[])
{
	int max,i,index;
	max=arr[0];
	for(i=1;i<ARR_SIZE;i++)
	{
		if(max<arr[i])
		{
			max=arr[i];
			index=i;
		}
	}
	printf("\narr[%d]=%d is the max.",index,max);
}

void minElement(int arr[])
{
	int min,i,index;
	min=arr[0];
	for(i=1;i<ARR_SIZE;i++)
	{
		if(min>arr[i])
		{
			min=arr[i];
			index=i;
		}
	}
	printf("\narr[%d]=%d is the min.",index,min);
}

void sumOfElements(int arr[])
{
	int sum=0,i;
	for(i=0;i<ARR_SIZE;i++)
	{
		sum+=arr[i];
	}
	printf("\nThe sum of all elements is =%d ",sum);
}

void displayArray(int arr[])
{
	int i;
	for(i=0;i<10;i++)
		{
			printf("\narr[%d]=%d",i,arr[i]);
		}
}