#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct book
{
	int id;
	char name[40];
	int price;
}book_t;

void merge_sort(book_t a[], int left, int right)
{
    int mid, i, j, k, n;
    book_t *temp;
    if(left == right || left > right)
      return;
     mid = (left + right) / 2; 
    merge_sort(a, left, mid);
    merge_sort(a, mid+1, right);
    n = right - left + 1;
    temp = (book_t*)malloc(n*sizeof(book_t));
    
    i = left;
    j = mid+1;
    k = 0;
    while(i <= mid && j<= right)
    {
        if(a[i].price > a[j].price)
        {
            temp[k] = a[i];
            i++;
            k++;
        }
        else
        {
            temp[k] = a[j];
            j++;
            k++;
        }
        
    } 
    while(i <= mid)
    {
        temp[k] = a[i];
        i++;
        k++;
    }
    while(j <= right)
    {
        temp[k] = a[j];
            j++;
            k++;
    }
    for(i=0;i<n;i++)
    {
        a[left + i] = temp[i];
        free(temp);
    }
}

int struct_cmp(const void*p, const void*q)
{
     return strcmp(((book_t*)p)->name, 
                  ((book_t*)q)->name); 
}

int main()
{
    book_t a[10] = { 
            {8, "Wings Of Fire", 700},
            {2, "Atlas Shrugged", 400},
            {5, "Twilight in Delhi", 244},
            {1, "The Fountainhead", 538},
            {10, "Rich Dad & Poor Dad", 984},
            {3, "The Secret", 245},
            {6, "Monk who sold his ferrari", 305},
            {9, "Chava", 658},
            {7, "The Alchemist", 794},
            {4, "The Blue Umbrella", 150}
    };
    int i, len = 10;
    merge_sort(a, 0, len-1);
    for(i=0;i<len;i++)
    printf("%d, %s, %d\n",a[i].id, a[i].name, a[i].price);
    printf("\n");

    printf("Unsorted Student Records:\n"); 
    for (i = 0; i < len; i++) { 
        printf("Id = %d, Name = %s, price = %d \n", 
               a[i].id, a[i].name, a[i].price); 
    } 

    qsort(a, len, sizeof(book_t), struct_cmp); 

    printf("\n\nStudent Records sorted by Name:\n"); 
    for (i = 0; i < len; i++) { 
        printf("Id = %d, Name = %s, price = %d \n", 
               a[i].id, a[i].name, a[i].price); 
    } 
    return 0;
}