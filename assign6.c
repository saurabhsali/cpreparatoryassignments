#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct student
{
    char name[30];
    int rollno;
    int std;
    int marks[6];
    char sub[6][30];
}student_t;

void accept_student(student_t *s)
{
    int i;
    printf("\nstd: ");
    scanf("%d",&s->std);

    printf("\nname:");
    scanf("%s",&s->name);

    printf("\nrollno :");
    scanf("%d",&s->rollno);
                
    for(i=1;i<=6;i++)
     {
         printf("Subject name: ");
         scanf("%s",&s->sub[i]);
         printf("marks: ");
         scanf("%d",&s->marks[i]);
              
     }
}

void display_student(student_t *s)
{
    int i;
    printf("\nName: %s, STD: %d, Rollno: %d\n",s->name,s->std,s->rollno);
    printf("\nsubject name :\tmarks");
    for(i=1;i<=6;i++)
    {
       printf("\n  %s \t     :\t %d",s->sub[i],s->marks[i]);
    }
}

#define SIZE	5

typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;

void cq_init(cirque_t *q)
{
     memset(q->arr,0,sizeof(q->arr));
     q->front= -1;
     q->rear= -1;
     q->count=0;
}

void cq_push(cirque_t *q,student_t s)
{
    q->rear = (q->rear+1) % SIZE;
    q->arr[q->rear] = s;
    q->count++;
}

void cq_pop(cirque_t *q)
{
    q->front = (q->front+1) % SIZE;
    q->count--;

}

student_t cq_peek(cirque_t *q) 
{
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

int cq_empty(cirque_t *q) 
{
	return q->count == 0;
}

int cq_full(cirque_t *q) 
{
	return q->count == SIZE;
}

int main()
{
    cirque_t q;
	student_t temp;
	cq_init(&q);
    int ch;
    do
	{
		printf("\n 0. exit\n");
        printf("\n 1. push\n");
        printf("\n 2. pop\n");
        printf("\n 3. peek\n");
        printf("\n Enter your choice\n");
        scanf("%d",&ch);

        switch(ch)
         {
        
                case 1:// push
                    if(cq_full(&q))
                        printf("queue is full.\n");
                    else 
                    {
                        accept_student(&temp);
                        cq_push(&q, temp);
                    }
                 break;

                case 2:// pop
                    if(cq_empty(&q))
                        printf("queue is empty.\n");
                    else 
                    {
                        temp = cq_peek(&q);
                        cq_pop(&q);
                        printf("popped: ");
                        display_student(&temp);
                    }
                        break;

                case 3: // peek
                    if(cq_empty(&q))
                        printf("queue is empty.\n");
                    else 
                    {
                        temp = cq_peek(&q);
                        printf("topmost: ");
                        display_student(&temp);
                    }
                    break;
        }

    }while(ch!=0);
}