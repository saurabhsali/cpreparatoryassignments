#include<stdio.h>
#include<stdlib.h>
#include<string.h> 

struct holder
{
 char name[10];
 char add[10];
 int contactNo;
}; 

struct account
{
 int id;
 char type[10];
 int balance;
 struct holder info;
}; 

struct node
{
 struct account Account;
 struct node *prev;
 struct node *next;
};

struct node *head=NULL; 

void add_first();
void add_last();
void display();
void findById();
void findByName();
void delete();

void main()
{ 
       int choice,ch;
       while(choice!=6)
       {
              printf("\n select operation you want to do");
              printf("\n1:Add First\n2:Add Last\n3:Dislpay All\n4:Find\n5:Delete\n6:Exit");
              scanf("%d",&choice);
              switch(choice)
              {
                     case 1:
                            add_first();
                            break;
                     case 2:
                            add_last();
                            break;
                     case 3:
                            display();
                            break;
                     case 4: 
                            printf("\n1:by id\n2:by name");
                            scanf("%d",&ch);
                            if(ch==1)
                                   findById();
                            else
                                   findByName();
                                   break;
                     case 5:
                            delete();
                            break;
                     case 6:
                            break;
              }
       }
} 

void add_first()
{
       int id,balance,contact;
       char type[10],name[10],addr[10];
       struct node *ptr=(struct node*) malloc(sizeof(struct node));
       if(ptr==NULL) 
       {
              printf("\nOverflow");
       }
       else
       {
              printf("\nEnter the id type balance\n");
              scanf("%d%s%d",&id,type,&balance);
              printf("\nname address contactNo");
              scanf("%s%s%d",name,addr,&contact);
              ptr->Account.id=id;
              strcpy(ptr->Account.type,type);
              ptr->Account.balance=balance;
              strcpy(ptr->Account.info.name,name);
              strcpy(ptr->Account.info.add,addr);
              ptr->Account.info.contactNo=contact;
              if(head==NULL)
              {
                     ptr->next=NULL;
                     ptr->prev=NULL;
                     head=ptr;
              } 
              else
              {
                     ptr->prev=NULL;
                     ptr->next = head;
                     head->prev=ptr;
                     head=ptr;
              }
       } 
}

void add_last()
{
       struct node *ptr,*temp;
       int id,balance,contact;
       char type[10],name[10],addr[10];
       ptr=(struct node*)malloc(sizeof(struct node));
       if(ptr==NULL)
       {
              printf("\nOVERFLOW");
       }
       else
       {
              printf("\nEnter the id type balance\n");
              scanf("%d%s%d",&id,type,&balance);
              printf("\nname address contactNo");
              scanf("%s%s%d",name,addr,&contact);
              ptr->Account.id=id;
              strcpy(ptr->Account.type,type);
              ptr->Account.balance=balance;
              strcpy(ptr->Account.info.name,name);
              strcpy(ptr->Account.info.add,addr);
              ptr->Account.info.contactNo=contact;
              if(head==NULL)
              {
                     ptr->next = NULL;
                     ptr->prev = NULL; 
                     head = ptr;
              }
              else
              {
                     temp = head;
                     while(temp->next!=NULL)
                     {
                            temp = temp->next;
                     }
                     temp->next = ptr;
                     ptr ->prev=temp;
                     ptr->next = NULL;
              }

       }
       printf("\nnode inserted\n");
}

void display()
{
       struct node *temp;
       temp=head;
       if(temp==NULL)
       {
              printf("\naccount details are allready empty");
       }
       else
       {
              while(temp!=NULL)
              { 
                     printf("\n Details of account holder");
                     printf("\nId=%d\nType=%s\nbalance=%d",temp->Account.id,temp->Account.type,temp->Account.balance);
                     printf("\nname=%s\naddress=%s\ncontactNo=%d",temp->Account.info.name,temp->Account.info.add,temp->Account.info.contactNo);
                     temp=temp->next;
              }
       }
}

void findById()
{
       int id;
       printf("\nEnter the id no of account holder");
       scanf("%d",&id);
       struct node *temp;
       if(head==NULL)
       {
              printf("\nNo one present in the details");
       }
       else
       {
              temp=head;
              while(temp!=NULL)
              {
                     if(temp->Account.id==id)
                     {
                            printf("\n Details of account holder");
                            printf("\nId=%d\nType=%s\nbalance=%d",temp->Account.id,temp->Account.type,temp->Account.balance);
                            printf("\nname=%s\naddress=%s\ncontactNo=%d",temp->Account.info.name,temp->Account.info.add,temp->Account.info.contactNo);
                            return;
                     }
                     temp=temp->next; 
              }
              printf("\naccount holder with given id not found");
       }
}

void findByName()
{
       char name[10];
       printf("\nEnter the name of account holder");
       scanf("%s",name);
       struct node *temp;
       if(head==NULL)
       {
              printf("\nNo one present in the details");
       }
       else
       {
              temp=head;
              while(temp!=NULL)
              {
                     if(!strcmp(temp->Account.info.name,name))
                     {
                            printf("\n Details of account holder");
                            printf("\nId=%d\nType=%s\nbalance=%d",temp->Account.id,temp->Account.type,temp->Account.balance);
                            printf("\nname=%s\naddress=%s\ncontactNo=%d",temp->Account.info.name,temp->Account.info.add,temp->Account.info.contactNo);
                            return;
                     }   
                     temp=temp->next;
              }
              printf("\naccount holder with given name not found");
       }
}

void delete()
{
       struct node *temp;
       if(head==NULL)
       {
              printf("\nThe accounts are alreasdy empty");
       }
       else
       {
              while(head!=NULL)
              {
                     temp=head;
                     head=head->next;
                     free(temp);
                     temp=NULL;
                     break;
              } 
       }
       printf("\nall details are deleted");
}